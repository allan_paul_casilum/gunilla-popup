<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              gunila.com
 * @since             1.0.0
 * @package           Gunilla_Popup
 *
 * @wordpress-plugin
 * Plugin Name:       Gunilla Popup
 * Plugin URI:        gunilla.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            gunilla
 * Author URI:        gunila.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gunilla-popup
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GUNILLA_POPUP_VERSION', '1.0.0' );
/**
 * For autoloading classes
 * */
spl_autoload_register('gp_directory_autoload_class');
function gp_directory_autoload_class($class_name){
		if ( false !== strpos( $class_name, 'GP' ) ) {
	 $include_classes_dir = realpath( get_template_directory( __FILE__ ) ) . DIRECTORY_SEPARATOR;
	 $admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
	 $class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
	 if( file_exists($include_classes_dir . $class_file) ){
		 require_once $include_classes_dir . $class_file;
	 }
	 if( file_exists($admin_classes_dir . $class_file) ){
		 require_once $admin_classes_dir . $class_file;
	 }
 }
}
function gp_get_plugin_details(){
 // Check if get_plugins() function exists. This is required on the front end of the
 // site, since it is in a file that is normally only loaded in the admin.
 if ( ! function_exists( 'get_plugins' ) ) {
	 require_once ABSPATH . 'wp-admin/includes/plugin.php';
 }
 $ret = get_plugins();
 return $ret['gunilla-popup/gunilla-popup.php'];
}
function gp_get_text_domain(){
 $ret = gp_get_plugin_details();
 return $ret['TextDomain'];
}
function gp_get_plugin_dir(){
 return plugin_dir_path( __FILE__ );
}
function gp_get_plugin_url(){
 return plugin_dir_url( __FILE__ );
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gunilla-popup-activator.php
 */
function activate_gunilla_popup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gunilla-popup-activator.php';
	Gunilla_Popup_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gunilla-popup-deactivator.php
 */
function deactivate_gunilla_popup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gunilla-popup-deactivator.php';
	Gunilla_Popup_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gunilla_popup' );
register_deactivation_hook( __FILE__, 'deactivate_gunilla_popup' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gunilla-popup.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gunilla_popup() {

	$plugin = new Gunilla_Popup();
	$plugin->run();

	if(is_admin()) {
		GP_Admin_Menu::get_instance();
	}

	GP_Modal::get_instance();
	GP_CouponAjax::get_instance();
	GP_WooCoupon::get_instance();
	//GP_ACF::get_instance()->init();
	//echo 'xxx'.GP_Session::get_instance()->getCoupon();
	//GP_Session::get_instance()->end_session();
}
//run_gunilla_popup();
add_action('plugins_loaded', 'run_gunilla_popup');

function init_gp() {
	GP_Admin_CptPopup::get_instance()->init();
	GP_Session::get_instance()->init();
	//GP_Session::get_instance()->setCoupon('test-session');
}
add_action('init', 'init_gp', 1);
