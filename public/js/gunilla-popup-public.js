(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 var gp_modal = function(){
 		return {
 			init:function(){
				$('#gp-modal').modal('toggle');
				var _accept = $('#gp-modal .gp-accept-coupon');
				var _footer = $('#gp-modal .modal-footer');
				//console.log('modal');
				_accept.on('click', function(e){
					e.preventDefault();
					var _coupon = $(this).data('coupon-code');

					_footer.find('.btn').hide();
					_footer.html('Please wait working to get your discount..');

					var data = [
 						{name: 'action', value: 'coupon_set_popup'},
 						{name: 'coupon', value: _coupon}
 					];
					$.ajax({
 						type: "POST",
 						url: frontendajax.ajaxurl,
 						data: data,
 					}).done(function( data ) {
						_footer.find('.btn').show();
 						$('#gp-modal').modal('hide');
						//_form[0].reset();
						//console.log(data);
 					});//ajax

				});
 			}//init
 		};
 	}();

 	$(window).load(function(){
 		gp_modal.init();
 	});
})( jQuery );
