<div class="bootstrap-iso">
  <!-- Modal -->
  <div class="modal fade" id="gp-modal" tabindex="-1" role="dialog" aria-labelledby="gp-modal" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered <?php echo $size;?>" role="document">
      <div class="modal-content" style="background:<?php echo $content_background;?>;background-image:url('<?php echo $content_background_image;?>');background-repeat: no-repeat;background-size: cover;">
        <?php if($show_header_title == 'yes') { ?>
          <div class="modal-header" style="background:<?php echo $header_background_color;?>">
            <h3 class="modal-title" id="exampleModalCenterTitle"><?php echo $header_title;?></h3>
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>-->
          </div>
        <?php } ?>
        <div class="modal-body">
          <?php echo $content;?>
          <?php if($form) { ?>
            <?php gravity_form($form['id'], false, false, false, '', true); ?>
          <?php } ?>
        </div>
        <div class="modal-footer">
          <p class="ajax-msg"></p>
          <button type="button" class="btn bnt-lg btn-secondary gp-decline-coupon" data-dismiss="modal"><?php echo $decline_button_text;?></button>
          <button type="button" class="btn bnt-lg btn-primary gp-accept-coupon" data-coupon-code="<?php echo $coupon->post_title;?>"><?php echo $accept_button_text;?></button>
        </div>
      </div>
    </div>
  </div>
</div>
