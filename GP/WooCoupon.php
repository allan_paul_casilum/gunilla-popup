<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class GP_WooCoupon {
	/**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()
	{
		add_action('woocommerce_before_cart', array($this, 'applyCoupon') );
		add_action('woocommerce_after_checkout_validation', array($this, 'gpCheckoutForFirsTimeCustomer'), 10, 3 );
		add_action('woocommerce_thankyou', array($this, 'gpThankYou'), 10, 1);
	}

	public function checkIfUserHasCompletedPayment($user_id = null)
	{
		if(is_null($user_id)){
			if(is_user_logged_in()){
				$current_user = wp_get_current_user();
				$user_id = $current_user->ID;
			}
		}
		if(!is_null($user_id)){
			$arr = array(
				'numberposts' => -1,
				'meta_key'    => '_customer_user',
				'meta_value'  => $user_id,
				'post_type'   => 'shop_order', // WC orders post type
				'post_status' => 'wc-completed' // Only orders with status "completed"
			);
			$customer_orders = get_posts($arr);

			if( count($customer_orders) >= 1) {
				return true;
			}
		}

		return false;
	}

	public function gpCheckoutForFirsTimeCustomer($posted)
	{
		global $woocommerce;
		/*print_r($_POST);
		print_r($woocommerce);
		print_r($posted);*/
		$checkout_email = $posted['billing_email'];

		$username = username_exists($checkout_email);

		$user = get_user_by( 'email', $checkout_email );

		if ( is_user_logged_in() ) {
			$check_user_purchased_success = $this->checkIfUserHasCompletedPayment($user->ID);
		}
		//echo $check_user_purchased_success ? 'y':'n';
		if($check_user_purchased_success){
		//if($username || $check_user_purchased_success){
			$msg = 'Sorry, Only first time customer only can avail this discount';
			wc_add_notice( $msg, 'error');
			wc_print_notices();
		}

	}

	public function applyCoupon()
	{
		$coupon =  GP_Session::get_instance()->getCoupon();
		if($coupon){
			if ( WC()->cart->has_discount( $coupon ) ) return;
	    WC()->cart->add_discount( $coupon );
	    wc_print_notices();
		}

	}

	public function gpThankYou($order_id)
	{
		GP_Session::get_instance()->end_session();
	}

}
