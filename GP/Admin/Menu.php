<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class GP_Admin_Menu {
	/**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()
	{
		add_action('admin_menu', array($this, 'admin_menu') );
	}

	public function admin_menu(){

		add_menu_page(
			'Popup Settings',
			'Popup Settings',
			'moderate_comments',
			'PopupSettings',
			array(GP_Admin_Controller::get_instance(), 'controller'),
			'dashicons-welcome-learn-more',
			3
		);
	}

}
