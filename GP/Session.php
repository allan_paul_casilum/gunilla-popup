<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class GP_Session {
	/**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()
	{

	}

	public function init()
	{
		//add_action('init', array($this, 'start_session'), 1 );
		$this->start_session();
		add_action('wp_logout',array($this, 'end_session') );
		add_action('wp_login',array($this, 'end_session') );
		//add_action('end_session_action',array($this, 'end_session') );
	}

	public function start_session()
	{
		if(!session_id()) {
			session_start();
		}
	}

	public function end_session() {
		$this->setCoupon();
		session_destroy ();
	}

	public function setCoupon($val = '')
	{
		$_SESSION['gp-coupon-code'] = $val;
	}

	public function getCoupon()
	{
		$this->start_session();
		if(isset($_SESSION['gp-coupon-code']) && $_SESSION['gp-coupon-code'] != '') {
			return $_SESSION['gp-coupon-code'];
		}
		return false;
	}

}
