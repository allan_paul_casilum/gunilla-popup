<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class GP_Modal {
	/**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()
	{
		add_action('wp_footer', array($this, 'init') );
	}

	public function init()
	{
		global $post;

		if(is_page()) {
			$id = $post->ID;
		}elseif(is_tax('product_cat')) {
			$id = get_queried_object();
		}

		$gunilla_popup = get_field( "gunilla_popup", $id );
		$enable_popup = get_field( "enable_popup", $id );

		$popup_post_id = $gunilla_popup->ID;
		$background_image = get_field( "background_image", $popup_post_id );
		$header_title = get_field( "header_title", $popup_post_id );
		$show_header_title = get_field( "show_header", $popup_post_id );
		$header_background_color = get_field( "header_background_color", $popup_post_id );
		$content_background = get_field( "content_background", $popup_post_id );
		$content = get_field( "content", $popup_post_id );
		$form = get_field( "form", $popup_post_id );
		$coupon = get_field( "woo_coupon", $popup_post_id );
		$accept_button_text = get_field( "accept_button_text", $popup_post_id );
		$decline_button_text = get_field( "decline_button_text", $popup_post_id );
		$size = get_field( "size", $popup_post_id );
		$content_background_image = get_field( "content_background_image", $popup_post_id );

		$first_time_customer = GP_WooCoupon::get_instance()->checkIfUserHasCompletedPayment();
		if($gunilla_popup && $enable_popup == 'yes' && !$first_time_customer) {

			$data = [
				'gunilla_popup' => $gunilla_popup,
				'enable_popup' => $enable_popup,
				'background_image' => $background_image,
				'header_title' => $header_title,
				'show_header_title' => $show_header_title,
				'header_background_color' => $header_background_color,
				'content_background' => $content_background,
				'content' => $content,
				'form' => $form,
				'coupon' => $coupon,
				'accept_button_text' => $accept_button_text,
				'decline_button_text' => $decline_button_text,
				'size' => $size,
				'content_background_image' => $content_background_image,
			];
			$template = GP_View::get_instance()->public_part_partials('partials/modal.php');
			//print_r($data);
			GP_View::get_instance()->display($template, $data);
		}

		//exit();
	}

}
