<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class GP_CouponAjax {
	/**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()
	{
		add_action( 'wp_ajax_coupon_set_popup', array($this,'init') );
		add_action( 'wp_ajax_nopriv_coupon_set_popup', array($this,'init') );
	}

	public function init()
	{
		$coupon = isset($_POST['coupon']) ? $_POST['coupon'] : false;

		if($coupon){
			GP_Session::get_instance()->setCoupon($coupon);
		}

		wp_die();
	}

}
