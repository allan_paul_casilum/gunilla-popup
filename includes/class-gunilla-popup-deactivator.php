<?php

/**
 * Fired during plugin deactivation
 *
 * @link       gunila.com
 * @since      1.0.0
 *
 * @package    Gunilla_Popup
 * @subpackage Gunilla_Popup/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gunilla_Popup
 * @subpackage Gunilla_Popup/includes
 * @author     gunilla <gunilla@mail.com>
 */
class Gunilla_Popup_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
