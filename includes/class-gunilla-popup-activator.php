<?php

/**
 * Fired during plugin activation
 *
 * @link       gunila.com
 * @since      1.0.0
 *
 * @package    Gunilla_Popup
 * @subpackage Gunilla_Popup/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gunilla_Popup
 * @subpackage Gunilla_Popup/includes
 * @author     gunilla <gunilla@mail.com>
 */
class Gunilla_Popup_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
